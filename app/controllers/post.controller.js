const db = require("../models");
const Post = db.posts;
const Op = db.Sequelize.Op;

//membuat method untuk create
exports.create = (req, res) =>{
    //kalau inputannya kosong kita tolak
    if(!req.body.title){
        res.status(400).send({
            message: "Content can not be empty"
        });
        return;
    }

    //kalau datanya ada baru kita proses 
    //create post
    const post ={
        title: req.body.title,
        description: req.body.description,
        published: req.body.published ? req.body.published : false
    }

    Post.create(post)
        .then((result)=>{
            res.send(data);
        }).catch((err)=>{
            res.status(500).send({
                message:
                    err.message || "Some error occurred while creating the Post"
            });
        });


};

//mmbuat methode-methode nya Retriver all
exports.findAll = (req, res)=>{
    const title = req.query.title;
    let condition = title?{ title: { [Op.like]: '%${title}%'}} : null;

    Post.findAll({where: condition})
    .then((data) => {
        res.send(data);
    }).catch((err) =>{
        res.status(500).send({
            message:
            err.message || "Some error occurred while find post"
        });
    });

};
// find a single
exports.findOne =(req, res) =>{
    const id = req.params.id; //punya parameter id untuk kita cari datanya

    Post.findByPk(id)
        .then((data) =>{
            res.send(data); //kalau misalkan hasilnya dapat, kita bakal kirim hasilnya ke variable data
        }).catch((err) =>{
            res.status(500).send({
                message:"Error retrieving post with id=" + id
            });
        });
};

//update post with id
exports.update = (req, res) =>{
    const id = req.params.id; //nerima data params.id

    Post.update(req.body, {
        where:{id:id}
    }).then((result) =>{
        if(result==1){ //berhasil update
            res.send({
                message:"Post was update successfully"
            });
        } else{
            res.send({
                message: "Cannot update post with id=${id}."
            })
        }
    }).catch((err) =>{ //kalau misalkan ada error lain didalam catchnya/internal/backendnya
        res.status(500).send({
            message:"Error updating post with id=" +id
        })
    });
};

//delete a post
exports.delete = (req, res) =>{
    const id = req.params.id;

    Post.destroy({
        where:{id:id}
    }).then((result) =>{
        if(result == 1){
            res.send({
                message:"Post was deleted successfully"
            })
        } else{
            res.send({
                message:"Cannot delete post with id=$(id)"
            })
        }
    }).catch((err) => {
        res.status(500).send({
            message:"Could not delete post with id=" +id
        })
    });

};

//delete all post
exports.deleteAll = (req, res) =>{
    Post.destroy({
        where:{},
        truncate: false
    }).then((result) =>{
        if(result == 1){
            res.send({
                message:"Post was deleted successfully"
            })
        } else{
            res.send({
                message:"Cannot delete post with id=$(id)"
            })
        }
    }).catch((err) => {
        res.status(500).send({
            message:"Could not delete post with id=" +id
        })
    });

};

//find all published
//exports.findAllPublished =(req, res) =>{


//};