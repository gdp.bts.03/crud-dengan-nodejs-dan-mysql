module.exports = {
    HOST: "localhost",
    USER: "root",
    PASSWORD: "",
    DB: "node_mysql",
    dialect: "mysql", //mau querynya pake siapa
    pool: { //time limit untuk koneksi
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    }

}