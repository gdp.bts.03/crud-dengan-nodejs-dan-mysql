module.exports = app => {
    const posts = require("../controllers/post.controller.js");

    let router = require("express").Router();

    //create new post
    router.post("/",posts.create);//method apa yang ada di controllernya

    //retrive all post
    router.get("/", posts.findAll);

    //retrive sigle post findone
    router.get("/:id", posts.findOne);

    //update post
    router.put("/:id", posts.update);

    //delete post
    router.delete("/:id", posts.delete);

    //delete all post
    router.delete("/", posts.deleteAll)

    app.use("/api/posts", router); //untuk akses ke kontroller postsnya
}