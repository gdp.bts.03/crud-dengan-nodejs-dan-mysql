const express = require("express"); 
const bodyParser = require("body-parser"); //mempermudah untuk kita ngolah request yang dikirim oleh client
const cors = require("cors");

//models
const db = require("./app/models");

const app = express(); //variable app akan menyimpan project express

//config untuk corsnya
let whiteList = [ //disini untuk ngatur mau pakai client di domain apa aja yang bisa ngakses rest api ini 
 'http://localhost:8081' //ini port punya client yang bisa akses rest api ini
];
let corsOption = {
    origin: function(origin, callback){
        if(whiteList.indexOf(origin) !== -1 || !origin){ //untuk nentuin client mana aja yang bisa pake restfull api ini
            callback(null, true);
        } else{
            callback(new Error('Not allowed by CORS'))//kalau origin/ client nya itu ga termasuk ke whitelist, kita bakal ngeblok jadi gaboleh akses kesini
        }
    }
};

//untuk pake
app.use(cors(corsOption));


//si server ini bisa nerima parse data atau parse request dalam bentuk aplication/json  x-www-form-urlencode
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended : true}));

db.sequelize.sync();

//sample route
app.get("/", (req, res) => { //api employe pake / atau root
    res.json({ //response berbentuk json
        message: "Welcome to restapi"
    });
});

//posts router
require("./app/routes/post.routes")(app); //(app)

//aktifin root/server
const PORT = process.env.PORT || 8080;

app.listen(PORT, () =>{
    console.log('Server is running on http://localhost:8080')
});